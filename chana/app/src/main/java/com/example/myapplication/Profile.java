package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }


    public void ig(View view) {
        Intent ig = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/g.gannnn/"));
        startActivity(ig);
    }

    public void gallery(View view) {
        Intent gallery = new Intent(this,Gallery.class);
        startActivity(gallery);
    }

    public void Home(View view) {
        Intent Home = new Intent(this,Home.class);
        startActivity(Home);
    }
}